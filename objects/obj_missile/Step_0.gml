/// @DnDAction : YoYo Games.Movement.Set_Direction_Point
/// @DnDVersion : 1
/// @DnDHash : 07E012EC
/// @DnDArgument : "x" "obj_enemy.x"
/// @DnDArgument : "y" "obj_enemy.y"
direction = point_direction(x, y, obj_enemy.x, obj_enemy.y);

/// @DnDAction : YoYo Games.Movement.Set_Speed
/// @DnDVersion : 1
/// @DnDHash : 6D7FA31D
/// @DnDArgument : "speed" "4"
speed = 4;